
/*

  Project:      Escape Room Puzzle #2 - Pentagram Box
  Student:      Nicole Vella , Vicky, Darini, Allan, Kyle
  Course:       DGIF 2002 - Physical Computing - OCAD University   
  Created on:    December 3, 2019

*/

// DC motor attached to pins 2 and 3
const int motor1A = 2; // motor A on digital pin 2
const int motor1B = 3; // motor B on digital pin 3

// time for the pause/delay between button press dection, helps to avoid "double presses"
const int pause = 400;

// buttons are on analog pins, which can be called with #'s 14-19 for A0 through A5
const int button1 = 15;
const int button2 = 16;
const int button3 = 17;
const int button4 = 18;
const int button5 = 19;

// pins for RGB LED indicators, green LED is not connected
const int led1blue = 4;
const int led1red  = 5;
const int led2blue = 6;
const int led2red  = 7;
const int led3blue = 8;
const int led3red  = 9;
const int led4blue = 10;
const int led4red  = 11;
const int led5blue = 12;
const int led5red  = 13;

// set button states to off
int button1State = 0;
int button2State = 0;
int button3State = 0;
int button4State = 0;
int button5State = 0;
int button1LastState = 0;
int button2LastState = 0;
int button3LastState = 0;
int button4LastState = 0;
int button5LastState = 0;

// string to caputre button presses and create buttonPressCode
String buttonPressCode = "";

// string to match to  win
String winningCode = "12345";

void setup() {

  // init motor
  pinMode(motor1A, OUTPUT);
  pinMode(motor1B, OUTPUT);

  // init input buttons
  pinMode(button1, INPUT);
  pinMode(button2, INPUT);
  pinMode(button3, INPUT);
  pinMode(button4, INPUT);
  pinMode(button5, INPUT);

  // init LED outputs  and turn them all blue
  pinMode(led1red, OUTPUT);
  pinMode(led1blue, OUTPUT);
  pinMode(led2red, OUTPUT);
  pinMode(led2blue, OUTPUT);
  pinMode(led3red, OUTPUT);
  pinMode(led3blue, OUTPUT);
  pinMode(led4red, OUTPUT);
  pinMode(led4blue, OUTPUT);
  pinMode(led5red, OUTPUT);
  pinMode(led5blue, OUTPUT);
  digitalWrite(led1red, LOW);
  digitalWrite(led1blue, HIGH);
  digitalWrite(led2red, LOW);
  digitalWrite(led2blue, HIGH);
  digitalWrite(led3red, LOW);
  digitalWrite(led3blue, HIGH);
  digitalWrite(led4red, LOW);
  digitalWrite(led4blue, HIGH);
  digitalWrite(led5red, LOW);
  digitalWrite(led5blue, HIGH);

}

void loop() {

  // get current state of all buttons
  button1State = digitalRead(button1);
  button2State = digitalRead(button2);
  button3State = digitalRead(button3);
  button4State = digitalRead(button4);
  button5State = digitalRead(button5);

  // update the indicator LEDs
  updateIndicator();

  // if total amount buttons pressed is not 5
  if (buttonPressCode.length() != 5) {

    // user pressed button 1
    if (digitalRead(button1) == HIGH) {

      //  check if current state = prev state to avoid holding button down giving a constant signal
      if (button1State != button1LastState) {

        // append the button number pressed to the end of the buttonPressCode string
        buttonPressCode += 1;

        // give a clue as to the button number by changing color of it's corresponding LED when it's being pressed
        digitalWrite(led1red, HIGH);

        // reset button state
        button1LastState = button1State;

        // delay for better button press tracking
        delay(pause);
      }
    }

    // same as above, but for button 2
    if (digitalRead(button2) == HIGH) {
      if (button2State != button2LastState) {
        buttonPressCode += 2;
        digitalWrite(led2red, HIGH);
        button2LastState = button2State;
        delay(pause);
      }
    }

    // same as above, but for button 3
    if (digitalRead(button3) == HIGH) {
      if (button3State != button3LastState) {
        buttonPressCode += 3;
        Serial.println(buttonPressCode);
        button3LastState = button3State;
        digitalWrite(led3red, HIGH);
        delay(pause);
      }
    }

    // same as above, but for button 4
    if (digitalRead(button4) == HIGH) {
      if (button4State != button4LastState) {
        buttonPressCode += 4;
        Serial.println(buttonPressCode);
        button4LastState = button4State;
        digitalWrite(led4red, HIGH);
        delay(pause);
      }
    }

    // same as above, but for button 5
    if (digitalRead(button5) == HIGH) {
      if (button5State != button5LastState) {
        buttonPressCode += 5;
        Serial.println(buttonPressCode);
        button5LastState = button5State;
        digitalWrite(led5red, HIGH);
        delay(pause);
      }
    }
  }
}

// function which checks how many buttons have beeen pressed by counting the length of the buttonPressCode string and updates the indicator LEDs based on # of button presses
void updateIndicator() {

  if (buttonPressCode.length() == 1) {

    // turn first LED red after first button press
    digitalWrite(led1red, HIGH);
    digitalWrite(led1blue, LOW);

  } else if (buttonPressCode.length() == 2) {

    // turn second LED red after second button press
    digitalWrite(led2red, HIGH);
    digitalWrite(led2blue, LOW);

  } else if (buttonPressCode.length() == 3) {

    // turn third LED red after third button press
    digitalWrite(led3red, HIGH);
    digitalWrite(led3blue, LOW);

  } else if (buttonPressCode.length() == 4) {

    // turn fourth LED red after fourth button press
    digitalWrite(led4red, HIGH);
    digitalWrite(led4blue, LOW);

  } else if (buttonPressCode.length() == 5) {

    // after 5 presses, check to see if the user won
    didUserWin();

  }
}

// function that checks if the user input the buttons in the right order
void didUserWin() {

  if (buttonPressCode == winningCode) {
    
    userWon();
  
  } else {

    userLost();
  
  }
}

// function for when user wins;
void userWon() {

  // Turn all lights pink
  digitalWrite(led1red, LOW);
  digitalWrite(led1blue, LOW);
  digitalWrite(led2red, LOW);
  digitalWrite(led2blue, LOW);
  digitalWrite(led3red, LOW);
  digitalWrite(led3blue, LOW);
  digitalWrite(led4red, LOW);
  digitalWrite(led4blue, LOW);
  digitalWrite(led5red, LOW);
  digitalWrite(led5blue, LOW);
  
  // reset the buttonPressCode and indicator LEDs
  buttonPressCode = "";
  
  // a function to unlock the wand
  unlockWand();

}

// function for when user loses;
void userLost() {

  // turn all light blue and reset
  digitalWrite(led1red, LOW);
  digitalWrite(led1blue, HIGH);
  digitalWrite(led2red, LOW);
  digitalWrite(led2blue, HIGH);
  digitalWrite(led3red, LOW);
  digitalWrite(led3blue, HIGH);
  digitalWrite(led4red, LOW);
  digitalWrite(led4blue, HIGH);
  digitalWrite(led5red, LOW);
  digitalWrite(led5blue, HIGH);

  // reset buttonPressCode and indicator LEDs
  buttonPressCode = "";
  
  // try again
  loop();

}

// function for unlocking the wand from the box;
void unlockWand() {

  // spin the motor for one second to wind up the string locking the wand to the box
  digitalWrite(motor1A, HIGH);
  digitalWrite(motor1B, LOW);
  delay(1000);
  digitalWrite(motor1A, LOW);
  digitalWrite(motor1B, LOW);

} 