
/*

  Project:      Escape Room Puzzle #3 - Skull & Wand
  Student:      Nicole Vella, Vicky, Darini, Allan, Kyle
  Course:       DGIF 2002 - Physical Computing - OCAD University
  Created on:   December 3, 2019
  Based on:     Week 10 Lesson, Lindy Wilkins DGIF 2002 Class
                Strandtest example code from Adafruit_NeoPixel.h library

*/

#include <Adafruit_NeoPixel.h> // include the neopixel library
#define LED_PIN   10 // pin connected to neopixel
#define LED_COUNT 8 // total LEDs on strip
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN); // declare the pixel strip

const int leftEye = 4; // photosensor connected to pin 4 placed inside left eye
const int rightEye = 5; // photosensor connected to pin 5 placed inside right eye

const int lengthOfZap = 200; // time the player must hold/zap the sensor in the eye for it to trigger a win

String didWin = "no"; // a string to hold win/lose status of player 

void setup() {
  
  strip.begin();           // init pixel strip
  strip.setBrightness(50); // set brightness
  strip.show();            // turn off all pixels

}

void loop() {

  delay(lengthOfZap);

  youLose(); // a function that runs by default, or if the user

  if (didWin == "no") {
    
    // detect brightness, if above 920, the wand laser is hitting the photosensor
    if (analogRead(leftEye) > 920 || analogRead(rightEye) > 920 ) {
    
      didWin = "yes"; // update variable to indicate user won
      youWin(10); // function that plays when user wins, cycles through colours
    
    }
  } 
}

void youLose() {
  
  for (int i = 0; i < strip.numPixels(); i++) {
  
    strip.setPixelColor(i, strip.Color(255, 0, 0));
    strip.show();
  
  }
}

// a function that cycles through colors, indicating the player winning the game
// takes one argument which controls the speed of the cycle
void youWin(int pause) {

  // a loop that runs for a period of time so that the game can reset automatically after a win
  for (int x = 0; x < 50; x++) {

    // cycle through all pixels in the strip, setting each to red
    for (int i = 0; i < strip.numPixels(); i++) {
      strip.setPixelColor(i, strip.Color(255, 0, 0));
      strip.show();
      delay(pause);
    }

    // cycle through all pixels in the strip, setting each to orange
    for (int i = 0; i < strip.numPixels(); i++) {
      strip.setPixelColor(i, strip.Color(255, 90, 0));
      strip.show();
      delay(pause);
    }

    // cycle through all pixels in the strip, setting each to yello
    for (int i = 0; i < strip.numPixels(); i++) {
      strip.setPixelColor(i, strip.Color(255, 255, 0));
      strip.show();
      delay(pause);
    }

    // cycle through all pixels in the strip, setting each to green
    for (int i = 0; i < strip.numPixels(); i++) {
      strip.setPixelColor(i, strip.Color(0, 255, 0));
      strip.show();
      delay(pause);
    }

    // cycle through all pixels in the strip, setting each to blue
    for (int i = 0; i < strip.numPixels(); i++) {
      strip.setPixelColor(i, strip.Color(0, 0, 255));
      strip.show();
      delay(pause);
    }

    // cycle through all pixels in the strip, setting each to purple
    for (int i = 0; i < strip.numPixels(); i++) {
      strip.setPixelColor(i, strip.Color(128, 0, 255));
      strip.show();
      delay(pause);
    }
  }
  
  didWin = "no"; // after the color cycle, reset the game
  loop(); // start the game over again

}